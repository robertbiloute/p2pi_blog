(package-initialize)
(require 'org)
(require 'ox-publish)
(require 'ox-latex)
(require 'ox-html)

(setq org-image-actual-width nil)

;; tells org to not care about timestamps: publish whatever the file timestamp is
(setq org-publish-use-timestamps-flag nil)

(setq user-full-name "Johnny B. Goode")

(setq org-html-postamble-format '(("en" "<p class=\"author\">Author: %a </p>                                                                                                                                                                                               
<p class=\"date\">Date: %d</p>                                                                                                                                                                                                                
<p class=\"creator\">Propulsé par: %c</p>")))


(setq org-startup-with-inline-images t)



(setq org-publish-project-alist
      `(("blog_p2pi"
	 :auto-sitemap t
	 :sitemap-title "p2pi"
	 :sitemap-sort-files chronologically
	 :with-author "Jonny B. Goode"
	 :makeindex t
	 :recursive t
	 :with-latex t
	;:auto-preamble t
	 :html-postamble t
					;:html-inline-images t
	 :stylesheet "./static/style.css"
	 :base-directory "."
	 :base-extension "org"
	 ;;:preparation-function delete-files-in-dir
	 ;:completion-function publish-p2pi-static
	 :publishing-directory "./public" ;"~/data/perso/prog/PhasedDongles/tmp/" ;
	 :publishing-function org-html-publish-to-html
	 :section-numbers nil
	 :with-toc t
	 :html-inline-images t
	 ;; :html-head ,nico-website-html-head
	 ;; :html-preamble ,nico-website-html-preamble
	 ;; :html-postamble ,nico-website-html-postamble
	 )

	("p2pi-static"
	 :with-author "Jonny B. Goode"
	 :base-directory "./static"
	 :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
	 :publishing-directory "./public/static"
	 :recursive t
	 :publishing-function org-publish-attachment
	  )

	("p2pi" :components ("blog_p2pi" "p2pi-static"))
	
	;; ("myprojectorg"
	;;  :base-directory "/home/censier/data/perso/DaHouse/"
	;;  :publishing-directory "/rsync:root@emplug:/home/robert/data/www/DaHouse/"
	;;  :publishing-function org-html-publish-to-html
	;;  :auto-preamble t
	;;  )

	))

